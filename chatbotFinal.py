import math
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import nltk
from nltk.stem.lancaster import LancasterStemmer #Sirve para quitar palabras innecesarias para que sea más entendible para el chatbot
stemmer=LancasterStemmer() #stemmer es un objeto de la clase LancasterStemmer
import tflearn
from tensorflow.python.framework import ops 
import tensorflow
import json
import random
import pickle
#nltk.download('punkt')

class Grafo: 
    def __init__(self):
      self.nodos = []
      self.matriz = [[None]*0 for i in range (0)]  #lista por comprensión

    def esta_en_nodos(self, v): 
      if self.nodos.count(v)==0:
        return False
      else: 
        return True
    
    def agregar_nodos(self, v):
      if self.esta_en_nodos(v):
        return False
      else: 
        self.nodos.append(v)  #si no está contenido se añade
    
      filas=columnas=len(self.matriz)
      matriz_aux=[[None]*(filas+1) for i in range(columnas+1)] #creo matriz auxiliar con espacios vacios de los nodos
    
      #Recorro matriz y copio contenido en matriz auxiliar
      for i in range(filas):
        for j in range(columnas):
          matriz_aux[i][j]=self.matriz[i][j]
      
      self.matriz=matriz_aux
      return True

    #La siguiente función: agraga valor de la arista en la matriz de adyacencia    
    # si dirigida es verdadero, se crea una arista de incio a fin
    def agregar_aristas(self,inicio,fin,valor,dirigida):
      if not(self.esta_en_nodos(inicio)) or not(self.esta_en_nodos(fin)):
        return False
      else:
        self.matriz[self.nodos.index(inicio)][self.nodos.index(fin)]=valor
    
      #Si la arista es no dirigida, es decir, existe en un sentido y en otro
      if not dirigida: 
        self.matriz[self.nodos.index(fin)][self.nodos.index(inicio)]=valor
      else: 
        return True

    def imprimir_matriz(self,m):
      cadena="\n"+"\t"
    
      for j in range(len(m)):
        cadena+="\t" +str(self.nodos[j])
      
            
      cadena+="\n" + "\t"+ (("\t" +"-") * len(m))
    
      for  i in range(len(m)):
        cadena+="\n" +str(self.nodos[i])+" |"
    
        for j in range(len(m)): 
          if i==j and (m[i][j] is None or m[i][j]==0):
            cadena+="\t" + "\\"
          else:
            if m[i][j] is None: 
              cadena+="\t"+"X" #no hay conexión/arista
            elif math.isinf(m[i][j]): #verifica si el nº es infinito
              cadena+="\t"+"**"
            else:
              cadena+="\t"+str(m[i][j])
      cadena+="\n"
      print(cadena)
      

g = Grafo()
   

g.agregar_nodos("STT")
g.agregar_nodos("B01")
g.agregar_nodos("B04")
g.agregar_nodos("B06")
g.agregar_nodos("B07")
g.agregar_nodos("B08")
g.agregar_nodos("B09")
g.agregar_nodos("B10")
g.agregar_nodos("B15")
g.agregar_nodos("B21")
g.agregar_nodos("B22")
g.agregar_nodos("B23")
g.agregar_nodos("B17")
g.agregar_nodos("B34")
g.agregar_nodos("B25")
g.agregar_nodos("B27")
g.agregar_nodos("B28")
g.agregar_nodos("B30")
g.agregar_nodos("A01")
g.agregar_nodos("A04")
g.agregar_nodos("A05")
g.agregar_nodos("A10") 
g.agregar_nodos("A14") 
g.agregar_nodos("T01") 
g.agregar_nodos("T02") 
g.agregar_nodos("T45")
g.agregar_nodos("T06")  
g.agregar_nodos("T07") 
g.agregar_nodos("ENT")
g.agregar_nodos("ES1")  
g.agregar_nodos("ES2") 
g.agregar_nodos("ASE")  

g.agregar_aristas("STT","B22",2,True)
g.agregar_aristas("STT","B21",3,True)
g.agregar_aristas("B22","B34",1,True)
g.agregar_aristas("B22","B23",2,True)
g.agregar_aristas("B23","B01",1,True)
g.agregar_aristas("B23","B25",2,True)
g.agregar_aristas("B25","B30",1,True)
g.agregar_aristas("B25","B27",2,True)
g.agregar_aristas("B27","B28",1,True)
g.agregar_aristas("B21","B17",3,True)
g.agregar_aristas("B21","B07",1,True)
g.agregar_aristas("B17","B15",3,True)
g.agregar_aristas("B17","ASE",1,True)
g.agregar_aristas("B07","B08",3,True)
g.agregar_aristas("B07","B06",1,True)
g.agregar_aristas("B08","B09",3,True)
g.agregar_aristas("B09","B010",3,True)
g.agregar_aristas("B06","B04",1,True)

g.agregar_aristas("STT","ES1",0,True)    
g.agregar_aristas("ES1","A01",2,True)
g.agregar_aristas("ES1","A04",3,True)
g.agregar_aristas("A04","A14",1,True)
g.agregar_aristas("A04","A05",3,True)
g.agregar_aristas("A05","ASE",1,True)
g.agregar_aristas("A05","A10",3,True)

g.agregar_aristas("STT","ES1",0,True)
g.agregar_aristas("ES1","ES2",0,True)
g.agregar_aristas("ES2","ENT",1,True)    
g.agregar_aristas("ENT","T01",2,True)
g.agregar_aristas("T01","T02",1,True)
g.agregar_aristas("T02","T45",1,True)
g.agregar_aristas("T45","T06",1,True)
g.agregar_aristas("T45","T07",3,True)

g.imprimir_matriz(g.matriz)
        
DG=nx.DiGraph()


for i in range(len(g.nodos)):
    DG.add_node(g.nodos[i])
for i in range(len(g.nodos)):
    for j in range(len(g.nodos)):        
        if type(g.matriz[i][j]) is int:    
            DG.add_edge(g.nodos[i] ,g.nodos[j],direccion=g.matriz[i][j])   

def buscarengrafo(destino):
      
    haspath=nx.has_path(DG, ("STT"), (destino))
    #if haspath==True:
     #   print('Hay camino') 
    path=nx.shortest_path(DG, source="STT", target=destino) #incluye los nodos por donde se pasa
    #print(path)
    inst=list()
    for x in range(0,len(path)-1):  
       inst.append(str(DG.get_edge_data(path[x],path[x+1])['direccion'])) #inclue las instrucciones a seguir
      
    return path, inst


"""Cargar datos"""

with open ("contenidoA.json", encoding='utf-8') as archivo: #Se abre contenido como archivo. utf-8 sirve para detectar caracteres como acentos
	datos=json.load(archivo) #Se guarda el contenido de archivo en la variable datos
#print ('Datos en el archivo de contenido:',datos)

#Se inicializan las listas que se van a implementar
	palabras=[] #Lista vacía de palabras
	tags=[] #Lista vacía de tags
	auxX=[]  
	auxY=[] 

	for contenido in datos ["contenido"]: #Accede a la parte contenido de la variable datos (tiene la info de contenido.json) y lo llama contenido
		for patrones in contenido ["patrones"]: #Accede a la parte patrones de la variable contenido (la acabo de definir en la linea anterior)
			auxPalabra=nltk.word_tokenize(patrones) #word_tokenize coge una frase y la separa en partes teniendo en cuenta signos como comas, puntos... Aquí coge los patrones y los separa en partes
			palabras.extend(auxPalabra) #Con extend añade las palabras sueltas de auxPalabra al final de la lista palabras
			auxX.append(auxPalabra) #Añade juntas las palabras de auxPalbra a la lista auxX
			auxY.append(contenido["tag"]) #En auxY se almacenan los tags almacenados en la variable contenido. Se almacenan 3 veces porque el for se recorre 3 veces

			if contenido ["tag"] not in tags: #Si ese tag aún no está en la lista vacía tags
				tags.append(contenido["tag"]) #Se añade

  
	print('Lista con sentencias de los patrones:',auxX) #Lista de patrones con listas para cada patrón
	print('Tags repetidos por el bucle:',auxY)
	print('Palabras incluidas en patrones:',palabras) #Lista de patrones con todas las palabras separadas  
	print('Tags del archivo contenido:',tags)
  
  #Para saber si una palabra está dentro de un conjunto
	palabras=[stemmer.stem(w.lower()) for w in palabras if w!="?"] #Se pasa a la clase stem todas las palabras de nuestra lista palabras en minúsculas, excluyendo ? para que el método stem adapte las palabras para que el chatbot entienda más fácil y se almacenan en palabras
	palabras=sorted(list(set(palabras))) #Ordena la lista de palabras
	tags=sorted(tags) #Ordena las etiquetas

	entrenamiento=[] #Lista vacía para el entrenamiento
	salida=[]

	salidaVacia=[0 for _ in range(len(tags))]

	for x, documento in enumerate(auxX): #Se recorre la lista auxX y se devuelve una palabra que se guarda en documento y su índice que se guarda en x
		cubeta=[]
		auxPalabra=[stemmer.stem(w.lower()) for w in documento] #Se pasa a la clase stem todas las palabras de la variable documento en minúsculas para que el método stem adapte las palabras para que el chatbot entienda más fácil y se almacenan en auxPalabra
		for w in palabras:
			if w in auxPalabra: #Si w está en auxPalabra
				cubeta.append(1) #Se pone 1 en la cubeta, indicando que ese elemento sí está
			else:
				cubeta.append(0)
		filaSalida=salidaVacia[:] 
		filaSalida[tags.index(auxY[x])]=1 #Se obtiene el contenido de auxY en cada índice x (se ha obtenido al recorrer auxX) y se busca en la lista tags
		entrenamiento.append(cubeta) #Si la palabra está en auxX (que después pasa documento y después a auxPalabra), sale un 1. Tener en cuenta que están ordenadas
		salida.append(filaSalida)
	#print('Entrenamiento:', entrenamiento)
	#print('Salida:', salida)

	entrenamiento=np.array(entrenamiento) #Se cambia de formato lista a formato array de numpy
	salida=np.array(salida)
	with open("variables.pickle", "wb") as archivoPickle: #Se crea el archivo variables.pickle y se abre en modo escritura
		pickle.dump((palabras, tags, entrenamiento, salida),archivoPickle) #Se guardan las variables en ese archivo

"""Crear red neuronal"""

ops.reset_default_graph() #Se pone el espacio de la red neuronal en blanco

red=tflearn.input_data(shape=[None,len(entrenamiento[0])]) #Entrada de datos a la red sin ninguna forma en particular y se indica cuantas entradas necesitamos, es decir la longitud de una fila de entrenamiento (nº palabras)
red=tflearn.fully_connected(red,10) #Fully connected indica que cada palabra está conectada con cada una de las neuronas. La red va atener 10 neuronas en la primera columna
red=tflearn.fully_connected(red,10) #Se añaden 10 más en otra columna. Todas las neuronas de la primera columna están conectadas con todas las neuronas de la 2ª columna
red=tflearn.fully_connected(red,len(salida[0]),activation="softmax") #El número de neuronas que se pone al final son las salidas que hay, la salida son los tags con los que sabemos qué responder (saludo y despedida). Todas las neuronas de la 2ª columna están conectadas con todas las de la salida
red=tflearn.regression(red) #Permite obtener probabilidades para saber a qué tag me estoy refiriendo

modelo=tflearn.DNN(red) #crear un modelo de la red
#try: #Para agilizar el proceso se carga el modelo del archivo modelo.tflearn
#modelo.load("modelo.tflearn")
#except: #Si es la primera vez que se ejecuta el chatbot, hay que crear ese archivo
modelo.fit(entrenamiento,salida,n_epoch=1000,batch_size=10,show_metric=True) #n_epoch es la cantidad de veces que el modelo va a ver los datos, cuanto mayor sea más fácil será que acierte a qué tag se refiere. batch_size indica cuantas entradas se van a ocupar (nº palabras en los patrones). El último parámetro es para mostrar el contenido de la red
modelo.save("modelo.tflearn") #Guardar el modelo en el archivo modelo.tflearn
#Al ejecutarlo, acc indica la credibilidad de la red neuronal, disminuye a medida que aumentan los patrones

def chatBot(): #Función
    while True: #Bucle infinito para que no deje de funcionar
        entrada=input("Usuario: ") #Espera la entrada del usuario
        if entrada=="apagar":
            break
        else:
            cubeta=[0 for _ in range(len(palabras))] #Se llena de ceros la cubeta que tiene longitud igual al número de palabras de los patrones
            entradaProcesada=nltk.word_tokenize(entrada) #Separa las palabras escritas por el usuario
            entradaProcesada=[stemmer.stem(palabra.lower()) for palabra in entradaProcesada]
            for palabraIndividual in entradaProcesada: #Se pasan a stem todas las palabras introducidas por el usuario en minúsculas para que las simplifiquefor palabraIndividual in entradaProcesada
                for i,palabra in enumerate (palabras): #Devuelve el elemento de la lista palabras y almacena en i su indice y en palabra la propia palabra
                    if palabra == palabraIndividual: #Si la palabra que me ha pasado el usuario (palabraIndividual) coincide con alguna de mi lista de palabras
                        cubeta[i]=1 #Se pone un 1 en la cubeta en esa posición
            resultados=modelo.predict([np.array(cubeta)]) #Probabilidad de a qué tag se refiere pasándole como argumento la cubeta para que sepa qué palabras hay 
            #print(resultados) #Aparecen dos números, el primero corresponde a la probabilidad de que sea una despedida (los tags están colocados en orden alafabético) y el segundo corresponde a la probabilidad de saludo
            mayor_prob=np.max(resultados) #Se queda con la probabilidad mayor
            resultadosIndices=np.argmax(resultados) #Indica el índice del tag que más probabilidad tenga           
 
            if mayor_prob<0.7: #Si la probabilidad de coincidencia con un tag es <0.5
                for tagAux in datos ["contenido"]: #Se van recorriendo los tags de contenido
                    if tagAux["tag"]=="escape":  #Si coincide con el tag de escape
                        respuesta=tagAux["respuestas"] #Se seleccionan como porsibles respuestas las de ese tag	
                        print("BOT: ", random.choice(respuesta)) #Se selecciona aleatoriamente una de las respuestas posibles                       
            else:
                tag=tags[resultadosIndices] #Se selecciona el tag con mayor probabilidad
                for tagAux in datos ["contenido"]: #Se van recorriendo los tags de contenido
                    if tagAux["tag"]==tag:  #Si coincide con el tag de mayor probabilidad
                        for n in range(0,len(g.nodos)):                            
                            if tagAux["tag"]==g.nodos[n]:
                                (path,inst)=buscarengrafo(g.nodos[n])
                                print(path)
                                print(inst)                                
                                cad=None
                                cad="\n"
                                for i in range(0,len(path)):                                     
                                    if i==(len(path)-1):
                                        for pathaux in datos ["contenido"]: #Se van recorriendo los tags de contenido
                                           if pathaux["tag"]==path[i]:#Si coincide con el tag de mayor probabilidad
                                                respuesta=pathaux["respuestas"]
                                                cad+="Y llegará a su destino: "+str(respuesta)
                                                break
                                    elif i <=(len(path)-2):
                                        for pathaux in datos ["contenido"]: #Se van recorriendo los tags de contenido
                                            if pathaux["tag"]==path[i]:  #Si coincide con el tag de mayor probabilidad    
                                                if i==0:
                                                  respuesta=pathaux["respuestas"]                              
                                                  cad+=str(respuesta)+","+" "
                                                  break 
                                                else:                                                   
                                                    respuesta=pathaux["respuestas"]                                                
                                                    if inst[i-1]==0:
                                                        cad+=str(respuesta)+","+" "
                                                    if "ES" in pathaux["tag"]:
                                                      cad+="Avance por la"+" "+str(respuesta)+","+" "
                                                    else:    
                                                        cad+="Pasará por"+" "+str(respuesta)+","+" "
                                                    break
                                        for instaux in datos ["contenido"]: #Se van recorriendo los tags de contenido
                                           if instaux["tag"]==inst[i]:  #Si coincide con el tag de mayor probabilidad
                                               respuesta=instaux["respuestas"]                                               
                                               cad+=str(respuesta)+","+" "
                                               break
                            
                                #cad+="\n"
                                characters = "'[]"
                                for x in range(len(characters)):
                                 cad = cad.replace(characters[x],"")
                                cad+="\n"
                                print("BOT: ", cad) #se imprime ruta a seguir
                                break
                            elif(n==(len(g.nodos)-1)):                                
                                respuesta=tagAux["respuestas"] #Se seleccionan como porsibles respuestas las de ese tag 
                                print("BOT: ", random.choice(respuesta)) #Se selecciona aleatoriamente una de las respuestas posibles                      
chatBot()
